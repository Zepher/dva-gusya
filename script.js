fetch('https://restcountries.eu/rest/v2/regionalbloc/eu')
.then((response) => {
    return response.json();

}
)
.then((resp) => {
    const listOfCapitals=document.createElement('ul');
    listOfCapitals.classList.add("category-items");
    resp.forEach(function(item){
        const listViewItem=document.createElement('li');
        listViewItem.appendChild(document.createTextNode(item.capital));
        listOfCapitals.appendChild(listViewItem);
    });
    document.getElementsByClassName("brends")[0].getElementsByClassName("col-5")[0].appendChild(listOfCapitals);

});